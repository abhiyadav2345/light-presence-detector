#include "light-presence-detector-app/light_presence_detector_app.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	light_presence_detector_init();

	while(1){
		light_presence_detector_loop();
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}
}
