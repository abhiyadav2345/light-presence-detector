/*
 * This file contains Pin Configurations for Team Thing Board v2.2
 * More Details about this board can be found at: https://mig.iquesters.com/?s=somthing&p=resources
 *
 * Please read the below comments carefully before using this file
 */

#ifndef THING_H_
#define THING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "som.h"
#define YOUR_THING_NAME	"Light Presence Detector"

/* ************************************** LED Button Selection ************************************
 * If the following LED(s) or Button(s) are to be used with the LED Helper or Button Helper module,
 * then do the following:
 * For LED Helper:
 *   - Change the value of the below MACRO LEDS_NUMBER to the desired number
 *   - Add the desired LED Macro Name(s) in the LEDS_LIST below
 * For Button Helper:
 *   - Change the value of the below MACRO BUTTONS_NUMBER to the desired number
 *   - Add the desired BUTTON Macro Name(s) in the BUTTONS_LIST below
 * */
#define BOARD_LED_1			SOM_PIN_43
#define BOARD_LED_2			SOM_PIN_73
#define BOARD_LED_3			SOM_PIN_97
#define BOARD_LED_4			SOM_PIN_55

#define BOARD_BUTTON_1		SOM_PIN_43
#define BOARD_BUTTON_2		SOM_PIN_73
#define BOARD_BUTTON_3		SOM_PIN_97
#define BOARD_BUTTON_4		SOM_PIN_54

/* ************************************** System LED Selection ************************************
 * The following LEDs are used by the EmMate Framework for showing various system specific notifications.
 * These are added to the LED Helper module. To add/remove more LEDs to the LED Helper modify the below
 * macros - LEDS_NUMBER & LEDS_LIST
 *
 * If the LED Helper and System HMI is enabled in the Menu Configuration tool, then system specific
 * notifications will be shown automatically, else these pins can be used for other purpose
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_100   /* Used by the EmMate Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_102   /* Used by the EmMate Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_104   /* Used by the EmMate Framework as BLUE LED */

#define LEDS_ACTIVE_STATE 	1	/*!< State at which a LED become active (High-1 or Low-0) */

/* Number of LEDs used for the LED Helper Module */
#define LEDS_NUMBER			3
/* List of LEDs to pass to the LED Helper Module. Must match the LEDS_NUMBER macro */
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

/* ************************************** System Button Selection ************************************
 * The following BUTTON is used by the EmMate Framework as a System/Factory Reset Button for completely erasing the
 * persistent memory. It is added to the Button Helper Module. To add/remove more Buttons to the Button Helper,
 * modify the below macros - BUTTONS_NUMBER & BUTTONS_LIST
 *
 * If the Button Helper and System HMI is enabled in the Menu Configuration tool, then the
 * Factory Reset feature will work, else this pin can be used for other purpose
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will
 */
#define SYSTEM_RESET_BUTTON		SOM_PIN_98		/* Used by the EmMate Framework as System/Factory Reset Button */

#define BUTTONS_ACTIVE_STATE	0	/* State at which a Button become active (High-1 or Low-0) */

/* Number of Buttons used for the Button Helper Module */
#define BUTTONS_NUMBER			1
/* List of Buttons to pass to the Button Helper Module. Must match the BUTTONS_NUMBER macro */
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

/********************************************** Light Detect Peripheral Board GPIO Selection *****************************************************/
// Attached Light Detect Peripheral Board(LDR) on MiKroBus Port 2
#define LIGHT_PRESENCE_DETECT_AN_GPIO		SOM_PIN_2

#endif /* THING_H_ */
