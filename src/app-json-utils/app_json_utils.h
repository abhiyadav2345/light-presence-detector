/*
 * app_json_utils.h
 *
 *  Created on: 12-Aug-2019
 *      Author: debashis
 */

#ifndef APP_APP_JSON_UTILS_H_
#define APP_APP_JSON_UTILS_H_

#include "../light-presence-detector-app/light_presence_detector_app.h"


core_err make_light_presence_publish_json(LIGHT_DETECTOR_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen);

core_err parse_config_json(char *string, LIGHT_DETECTOR_CONFIG_DATA *resp) ;



#endif /* APP_APP_JSON_UTILS_H_ */
