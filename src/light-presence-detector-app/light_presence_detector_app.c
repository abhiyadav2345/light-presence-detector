#include "light_presence_detector_app.h"
#include "../app-json-utils/app_json_utils.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "http_client_api.h"
#include "conn.h"
#include "appconfig_helper.h"
#include "apppostdata_helper.h"

#include "adc_core_channel.h"
#include "adc_core.h"

#define TAG "light_presence_detector_app"

#define ADC_READ_MIN_mVOLT	(float)150
#define ADC_READ_MAX_mVOLT	(float)2450

#define CONFIG_JSON	"{\"light_presence_read_interval\": 100, \"light_presence_publish_interval\":3000}"

LIGHT_DETECTOR_APP_DATA app_data;

static TaskHandle light_presence_publish_task_handle = NULL;

/****************************** Module's Static Functions ***********************************/

static core_err app_configuration_process(char *app_config, size_t app_config_len) {
	core_err res = CORE_FAIL;
	CORE_LOGI(TAG, "Application Configuration %s", app_config);
	if (app_config != NULL) {
		res = parse_config_json(app_config, &app_data.app_config_data);
		if (res != CORE_OK) {
			CORE_LOGE(TAG, "Failed to parse App Configuration");
		}
	}
	return res;
}

static void publish_task(void *param) {

	while (1) {
		char *pub_json_buff = NULL;
		size_t pub_json_buff_len = 0;
		core_err err = CORE_FAIL;

		err = make_light_presence_publish_json(&app_data.publish_data, &pub_json_buff, &pub_json_buff_len);
		if (err == CORE_OK) {
				CORE_LOGI(TAG, "\r\n-: Light Presence JSON :-\r\n%s\r\n", pub_json_buff);

//				if (get_network_conn_status() == NETCONNSTAT_CONNECTED) {

					core_err res = migcloud_post_app_json_http(pub_json_buff);
					if (res == CORE_OK) {
						// Do somthing else ...
					} else if (res == CORE_ERR_NO_MEM) {
						CORE_LOGE(TAG, "Publish Process Failed because of low memory");
					} else {
						CORE_LOGE(TAG, "Publish Failed");
					}
//				}
		}
		free(pub_json_buff);
		TaskDelay(app_data.app_config_data.light_presence_publish_interval / TICK_RATE_TO_MS);
	}

	CORE_LOGI(TAG, "Deleting %s", __func__);
	light_presence_publish_task_handle = NULL;
	TaskDelete(NULL);
}

static core_err init_light_presence_detector_gpio() {
	core_err ret = CORE_FAIL;

	/*
	 * Initialize the ADC with the configured data and start processing.
	 * ADC GPIO: LIGHT_PRESENCE_DETECT_AN_GPIO
	 * ADC read interval in seconds: 1secs = 1000millisec
	 * ADC read data width in bit: ADC_CORE_WIDTH_BIT_12
	 * ADC voltage attenuation level: ADC_CORE_ATTEN_DB_11 (ADC read max Volt 3.3v)
	 */
	ret = init_adc_peripheral(LIGHT_PRESENCE_DETECT_AN_GPIO, DELAY_1_SEC, ADC_CORE_WIDTH_BIT_12, ADC_CORE_ATTEN_DB_11);

	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Initialize the LDR Light Presence Detect Analog Data Pin");
	}

	return ret;
}

static void calculate_light_intensity() {
	int adc_raw = 0;
	uint32_t adc_mv = 0;
	core_err res = CORE_FAIL;

	/* Lux calculation
	 * Source: https://www.allaboutcircuits.com/projects/design-a-luxmeter-using-a-light-dependent-resistor/
	 * Here the values of m and b are based on the above link.
	 * In your case, you must obtain your own values as per the method suggested
	 * */
#define LUX_CALC_SCALAR		(12518931.7)
#define LUX_CALC_EXPONENT	(-1.4059)

	/* 1. Get the Resistor Voltage from the ADC */
	res = get_adc_peripheral_data(LIGHT_PRESENCE_DETECT_AN_GPIO, &adc_raw, &adc_mv);

	if (res == CORE_OK) {
//		CORE_LOGW(TAG, "adc_raw = %d , adc_V = %.2f", adc_raw, (float)adc_mv / 1000);

		/* 2. 	Calculate the LDR resistance from voltage divider formulae
		 * 		------------------------------
		 * 		| R(ldr) = (Vs/Vo * R2) - R2 |
		 * 		------------------------------
		 * 		where,
		 * 			R(ldr) = LDR resistance
		 * 			Vs=5V (as per Light Detect Peripheral Board schematic)
		 * 			V0 = measured voltage from ADC
		 * 			R2 = 4.7k (4700 ohm, as per Light Detect Peripheral Board schematic)
		 * */
#define SOURCE_VOLTAGE			5
#define DIVIDER_RESISTANCE_OHM	4700

		float adc_v = (float)adc_mv/1000;
		float Rldr = (SOURCE_VOLTAGE / adc_v * DIVIDER_RESISTANCE_OHM) - DIVIDER_RESISTANCE_OHM;
//		CORE_LOGW(TAG, "R(ldr) = %0.2f", Rldr);

		/* 3.	Calculate the LUX from the below formulae:
		 * 		-----------------------------------------
		 * 		| Lux = 10^b * R(ldr)^m, or				|
		 * 		-----------------------------------------
		 * 		| Lux = (1.25 * 10^7) * R(ldr)^-1.4059	|
		 * 		-----------------------------------------
		 * */
		float Lux = LUX_CALC_SCALAR * pow(Rldr, LUX_CALC_EXPONENT);
		CORE_LOGW(TAG, "Calculated Lux = %0.2f", Lux);

		app_data.publish_data.light_intensity = Lux;
	} else {
		CORE_LOGE(TAG, "Failed to read LPG_MQ5_AN_PIN");
	}
}

void light_presence_detector_init() {
	CORE_LOGI(TAG, "Configuring The Light Presence Detector Peripheral's GPIO");

	core_err err = init_light_presence_detector_gpio();
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to configure the input GPIO");
		return;
	}

	// register application configuration function
	migcloud_register_app_config_function(app_configuration_process);

	// default Light Presence Detector Publish interval
	app_data.app_config_data.light_presence_publish_interval = DELAY_10_SEC;

	bool status = TaskCreate(publish_task, "publish_task", TASK_STACK_SIZE_4K, NULL,
			THREAD_PRIORITY_5, &light_presence_publish_task_handle);
	if (status != true) {
		CORE_LOGE(TAG, "Failed to start Light Presence Publish Task");
	}
}

void light_presence_detector_loop() {
	calculate_light_intensity();
}
